# CodableContent

Codable extensions based on https://www.swiftbysundell.com/posts/type-inference-powered-serialization-in-swift?rq=decode


## Installation

### Carthage
```shell 
git "git@gitlab.com:thecb4/CodableContent.git" ~> 0.2.1
```

### Swift Package Manager
```shell 
.package(url: "https://gitlab.com/thecb4/contentcodable.git", .upToNextMinor(from: "0.1.0"))
```



## Decoding Usage
```swift
guard let user: User = try? data.decoded() else { 
  XCTFail("Unable to decode User")
  return 
}
```

## Encoding Usage
```swift
let user = EncodableUser(name: "Test", age: 10)

guard let expected = try? user.encoded() else {
  XCTFail("Unable to encode \(user) with DarkMatter protocol")
  return
}
```
