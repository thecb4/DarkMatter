//
//  DarkMatterError.swift
//  DarkMatter Errors
//
//  Created by Cavelle Benjamin on 19-Apr-09.
//  Copyright © 2019 The CB4. All rights reserved.
//

public enum DarkMatterError: Error {
  case stringToDataError(String)
  case undecodable(String)
  case unencodable(String)
}
