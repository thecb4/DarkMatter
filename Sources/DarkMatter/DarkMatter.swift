import Foundation

/**
  A protocol that combines Decodable, SelfEncodable, and Equatable

  - SeeAlso: Decodable
  - SeeAlso: SelfEncodable
  - SeeAlso: Equatable
*/
public protocol DarkMatter: Decodable & SelfEncodable & Equatable {}
