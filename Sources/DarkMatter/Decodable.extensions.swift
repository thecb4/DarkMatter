//
//  Decodable.extensions.swift
//  Decodable Extensions
//
//  Created by Cavelle Benjamin on 18-Dec-15 (50).
//  Copyright © 2018 The CB4. All rights reserved.
//

import Foundation

/**
  A Type that serves as a decoder from a satring.

  - SeeAlso: JSONDecoder
  - SeeAlso: PropertyListDecoder

*/
public protocol DecodableString {

  /**
    Decode content of given type directly from a string. The compiler infers type.

    - Parameters:
      - type: The type to decode
      - data: The data source to decode to tyoe T

    - Returns: The decoded type
  */
  func decoded<T: Decodable>(using decoder: ContentDecoder) throws -> T
}

extension String: DecodableString {

  public func decoded<T: Decodable>(using decoder: ContentDecoder = JSONDecoder()) throws -> T {

    guard let data = self.data(using: .utf8) else {
      throw DarkMatterError.stringToDataError(String(describing: self))
    }

    return try decoder.decode(T.self, from: data)
  }

}

/**
  A Type that serves as a decoder for any type of content.

  - SeeAlso: JSONDecoder
  - SeeAlso: PropertyListDecoder

*/
public protocol ContentDecoder {

  /**
    Decode content of given type. The compiler infers type.

    - Parameters:
      - type: The type to decode
      - data: The data source to decode to tyoe T

    - Returns: The decoded type
  */
  func decode<T: Decodable>(_ type: T.Type, from data: Data) throws -> T
}

extension JSONDecoder: ContentDecoder {

}

/**
  A Type that allows for inference for the type to decode to

  - SeeAlso: JSONDecoder
  - SeeAlso: PropertyListDecoder

*/
public protocol DecodableData {

  /**
    Decode content of given type. The compiler infers type.

    - Parameters:
      - decoder: The decoder to use to decode data to type

    - Returns: The decoded type
  */
  func decoded<T: Decodable>(using decoder: ContentDecoder) throws -> T
}

extension Data: DecodableData {
  public func decoded<T: Decodable>(using decoder: ContentDecoder = JSONDecoder()) throws -> T {
    return try decoder.decode(T.self, from: self)
  }
}
