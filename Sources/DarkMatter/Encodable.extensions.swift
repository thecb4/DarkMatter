//
//  Content.swift
//  NowThis
//
//  Created by Cavelle Benjamin on 18-Dec-15 (50).
//  Copyright © 2018 The CB4. All rights reserved.
//

import Foundation

/**
  A Type that serves as an encoder for any type of content.

  - SeeAlso: JSONDecoder
  - SeeAlso: PropertyListDecoder

*/
public protocol ContentEncoder {

  /**
    Encode content of given type..

    - Parameters:
      - value: The value to be encoded

    - Returns: The encoded data
  */  
  func encode<T: Encodable>(_ value: T) throws -> Data
}

extension JSONEncoder: ContentEncoder {

}

/**
  A Type that encode itself into data

  - SeeAlso: JSONEncder
  - SeeAlso: PropertyListEncoder
  - SeeAlso: ContentEncoder

*/
public protocol SelfEncodable: Encodable {

  /**
    Encode self with given ContentEncoder

    - Parameters:
      - encoder: The encoder to use to convert to data

    - Returns: Encoded data
  */
  func encoded(using encoder: ContentEncoder) throws -> Data
}

extension SelfEncodable {
  public func encoded(using encoder: ContentEncoder = JSONEncoder()) throws -> Data {
    return try encoder.encode(self)
  }
}
