#!/usr/bin/env bash

echo "=============== Running Swift on macOS (package) =============== " && \
swift test && \
echo "=============== Running Swift on Linux (package) =============== " && \
docker run \
  --privileged --rm -t \
  --name swift-test \
  --volume "$(pwd):/package" \
  --workdir "/package" \
  swift:4.2 \
  /bin/bash -c \
  "swift package resolve && swift test --build-path ./.build/linux" && \
  echo "=============== Running Swift on iOS (Carthage) ===============" && \
  xcodebuild clean test -scheme "DarkMatter-iOS" -destination "platform=iOS Simulator,name=iPhone XS"
