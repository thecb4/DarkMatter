import XCTest

#if !os(macOS)
public func allTests() -> [XCTestCaseEntry] {
    return [
        testCase(ContentDecodableTests.allTests),
        testCase(ContentEncodableTests.allTests)
    ]
}
#endif
