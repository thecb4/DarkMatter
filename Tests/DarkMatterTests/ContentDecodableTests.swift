import DarkMatter
import XCTest

struct DecodableUser {
  let name: String
  let age: Int
}

extension DecodableUser: DarkMatter {}

final class ContentDecodableTests: XCTestCase {

  func testJSONDataDecoderExtension() {

    let expected = DecodableUser(name: "Test", age: 10)

    guard let data = try? JSONEncoder().encode(expected) else {
      XCTFail("Unable to encode \(expected) with JSONEncoder")
      return
    }

    XCTAssertNotNil(data) // make sure you can encode the normal way

    guard let actual: DecodableUser  = try? data.decoded() else {
      XCTFail("Unable to decode using DataDecoder")
      return
    }

    XCTAssertEqual(expected, actual)

  }

  func testDecodeFromString() {

    let expectedString =
      """
      {
        "name": "John",
        "age": 10
      }
      """

    guard let expected = try? expectedString.decoded() as DecodableUser else {
      XCTFail("Unable to decode using string: \(expectedString)")
      return
    }

    guard let data = try? JSONEncoder().encode(expected) else {
      XCTFail("Unable to encode \(expected) with JSONEncoder")
      return
    }

    XCTAssertNotNil(data) // make sure you can encode the normal way

    guard let actual: DecodableUser  = try? data.decoded() else {
      XCTFail("Unable to decode using DataDecoder")
      return
    }

    XCTAssertEqual(expected, actual)

  }

  static var allTests = [
    ("testJSONDataDecoderExtension", testJSONDataDecoderExtension),
    ("testDecodeFromString", testDecodeFromString)
  ]
}
