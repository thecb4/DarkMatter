import DarkMatter
import XCTest

struct EncodableUser {
  let name: String
  let age: Int
}

extension EncodableUser: DarkMatter {}

final class ContentEncodableTests: XCTestCase {

  func testEncodable() {

    let user = EncodableUser(name: "Test", age: 10)

    guard let expected = try? JSONEncoder().encode(user) else {
      XCTFail("Unable to encode \(user) with JSONEncoder")
      return
    }

    guard let actual = try? user.encoded() else {
      XCTFail("Unable to encode \(user) with SelfEncoder protocol")
      return
    }

    XCTAssertEqual(expected, actual)

  }

  static var allTests = [
    ("testEncodable", testEncodable)
    // ("testContentDecodable", testContentDecodable)
  ]
}
