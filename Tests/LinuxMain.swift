import XCTest
import DarkMatterTests

var tests = [XCTestCaseEntry]()
tests += DarkMatterTests.allTests()
XCTMain(tests)
